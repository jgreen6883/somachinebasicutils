# SoMachineBasicUtils

A collection of utilities that I've written to help this program be a little more usable for my use cases.

SoMachine Basic is a utility from Schneider Electric to program small Modicon PLCs. Unfortunately, it doesn't re-address parts of the program very well when you re-use custom function blocks; you have to manually edit all the registers. Fortunately, the blocks are saved as xml, so I was able to write a small script that re-addresses the parts I needed. It's got some limitations, but it worked well for the time that I needed it.
