convert.py

A script that will re-address all the memory in a function block to another range of sequential memory 
addresses that coorelate with address symbols used in the function block.

usage: convert.py [-h, --help] -m address_type -s starting_address -u path/to/function/block [-p path/to/program_file]

where 'address_type' is one of:
	m  - memory bit,
	md - memory double,
	mf - memory float,
	kw - constant word,
	kd - constant double,
	kf - constant float,
	tm - timer,
	c - counter,

'starting_address' is where the addresses should start counting up from, and the path is to the function
block to edit.

The program file path is optional; adding this will clear the symbols used in the function block from any blocks
in the program file. If not included, they will have to be cleared manually.

The symbol text is a text file in the same folder with the block that has the same name but a '.symb' file extenstion; 
it contains a list of all symbols used in the block. The addresses are ordered based on this symbol ordering. This 
file needs to exist for the re-addressing to work properly.

Only the addresses specified by the type will be re-addressed; other types will be skipped. This can be run
multiple times if needed to adjust memory of different types.

## TODO ##

General cleanup and refactoring some methods and things; it's quite messy right now.

Currently, the values themselves for timers, constants, and counters need to be changed manually; this needs to be 
done automatically during the write back. I think I could create datatypes and store things easier than just using 
dictionaries. Then write back would hopefully be simpler.

Only M, KW, and TM types work so far to change addresses in the block. Other address types still need to be implemented
and tested.

Create a gui so you don't have to call it from the command line.
