#! /usr/bin/env python
# coding: utf-8

"""
convert.py

    Re-addresses udf blocks for Schneider Electric Machine Edition Basic 
    since it doesn't adjust imported blocks to new addresses automatically.
    
    If passed a path to a program file the block will be used in, it will
    clear tags used in the block from the program file.
    
"""

import sys
import os
import argparse as arg
import xml.etree.ElementTree as ET
import modicon as mod

USAGE = "usage: convert.py [-h] -m MEMORY_TYPE -s START_ADDR -u UDFB [-p PROG]"
SYMBOLS_EXT = ".symb"

parser = arg.ArgumentParser()
parser.add_argument('-m', '--memory_type', type=str, nargs=1, help='Memory type to edit', required=True)
parser.add_argument('-s', '--starting_address', type=int, nargs=1, help='Starting number for block addresses', required=True)
parser.add_argument('-u', '--udfb_file', type=str, nargs=1, help='Path to user-defined function block', required=True)
parser.add_argument('-p', '--program_file', type=str, nargs=1, help='Path to program file', required=False)
args = parser.parse_args()

BLOCK_FILENAME = args.udfb_file[0]
REGISTERS_TO_UPDATE = mod.get_memtype_for_user(args.memory_type[0].upper())
STARTING_ADDRESS = args.starting_address[0]
PROGRAM_FILENAME = args.program_file[0] if args.program_file else None

SYMBOLS_FILENAME, _ = os.path.splitext(BLOCK_FILENAME)
SYMBOLS = SYMBOLS_FILENAME + SYMBOLS_EXT

MEMTYPE_XPATH = {
    mod.MemoryType.MEMORY_BIT  : "./SoftwareConfiguration/MemoryBits/MemoryBit",
    mod.MemoryType.MEMORY_WORD : "./SoftwareConfiguration/MemoryWords/MemoryWord",
    mod.MemoryType.MEMORY_DOUBLE : "./SoftwareConfiguration/MemoryDoubleWords/MemoryDoubleWord",
    mod.MemoryType.MEMORY_FLOAT : "./SoftwareConfiguration/MemoryFloats/MemoryFloats",
    mod.MemoryType.CONSTANT_WORD : "./SoftwareConfiguration/ConstantWords/ConstantWord",
    mod.MemoryType.CONSTANT_DOUBLE : "./SoftwareConfiguration/ConstantDoubleWords/ConstantDoubleWord",
    mod.MemoryType.CONSTANT_FLOAT : "./SoftwareConfiguration/ConstantMemoryFloats/ConstantFloat",
    mod.MemoryType.TIMER : "./SoftwareConfiguration/Timers/TimerTM",
    mod.MemoryType.COUNTER : "./SoftwareConfiguration/Counters/Counter",
}

if not os.path.isfile(BLOCK_FILENAME):
    print(USAGE)
    print("convert.py: error: missing udf block at specified path: " + BLOCK_FILENAME)
    sys.exit()
if not os.path.isfile(SYMBOLS):
    print(USAGE)
    print("convert.py: error: missing symbols list at specified path: " + SYMBOLS)
    sys.exit()
if PROGRAM_FILENAME and not os.path.isfile(PROGRAM_FILENAME):
    print(USAGE)
    print("convert.py: error: missing program at specified path: " + PROGRAM_FILENAME)
    sys.exit()
if REGISTERS_TO_UPDATE not in mod.VALID_MEMTYPES:
    print(USAGE)
    print("convert.py: error: address type must be one of '" + ''.join(c.value+" " for c in mod.VALID_MEMTYPES)[:-1] + "': " + REGISTERS_TO_UPDATE)
    sys.exit()

def main():
    start_addr = STARTING_ADDRESS
    udfb_updated = False
    prog_updated = False
    symbols_newaddrs = {}
    symbols_oldaddrs = {}
    oldaddrs_newaddrs = {}
    
    try:
        tree = ET.parse(BLOCK_FILENAME)
    except ET.ParseError as e:
        print("Xml parsing error, stopping now: " + BLOCK_FILENAME)
        print(str(e))
        sys.exit()
    else:
        root = tree.getroot()
        for child in root.findall("./CustomSymbols/CustomSymbol"):
            addr = ""
            symbol = ""
            for node in child:
                if node.tag == "Address":                
                    addr = node.text
                elif node.tag == "Symbol":
                    symbol = node.text
            symbols_oldaddrs[symbol] = addr

    with open(SYMBOLS, "r") as f:
        for line in f:
            symbol = line.upper().strip()
            old_addr = symbols_oldaddrs[symbol]
            if mod.is_memory_bit(old_addr) and REGISTERS_TO_UPDATE == mod.MemoryType.MEMORY_BIT:
                memory_type = mod.MemoryType.MEMORY_BIT
            elif mod.is_timer(old_addr) and REGISTERS_TO_UPDATE == mod.MemoryType.TIMER:
                memory_type = mod.MemoryType.TIMER
            elif mod.is_constant_word(old_addr) and REGISTERS_TO_UPDATE == mod.MemoryType.CONSTANT_WORD:
                memory_type = mod.MemoryType.CONSTANT_WORD
            else:
                memory_type = None
            try:
                new_addr = mod.get_memory_prefix(memory_type) + str(start_addr)
            except mod.NoMemoryPrefixException as e:
                print(e)
                continue
            else:
                symbols_newaddrs[symbol] = new_addr
                oldaddrs_newaddrs[old_addr] = new_addr
                start_addr += 1
    
    if PROGRAM_FILENAME is not None:
        try:
            prog_tree = ET.parse(PROGRAM_FILENAME)
        except ET.ParseError as e:
            print("Xml parsing error, can't update " + PROGRAM_FILENAME)
            print(str(e))
        else:
            prog_root = prog_tree.getroot()
            for child in prog_root.findall(MEMTYPE_XPATH[REGISTERS_TO_UPDATE]):
                for node in child:
                    if node.tag == "Symbol" and node.text in symbols_newaddrs:
                        node.text = ""
                        prog_updated = True
                        for node in child:
                            if node.tag == "Value":
                                node.text = "0"

    for child in root.findall("./CustomSymbols/CustomSymbol"):
        for node in child:
            if node.tag == "Address" and node.text in oldaddrs_newaddrs:
                node.text = oldaddrs_newaddrs[node.text]
                udfb_updated = True
    
    for child in root.findall(".UserFunctionBlockPous/UserFunctionBlockPou/Rungs/RungEntity/LadderElements/LadderEntity"):
        for node in child:
            if node.tag == "Descriptor" and node.text in oldaddrs_newaddrs:
                node.text = oldaddrs_newaddrs[node.text]
                udfb_updated = True
    
    for child in root.findall(".UserFunctionBlockPous/UserFunctionBlockPou/Rungs/RungEntity/InstructionLines/InstructionLineEntity"):
        for node in child:
            if node.tag == "InstructionLine":
                for k, v in oldaddrs_newaddrs.items():
                    if k in node.text:
                        node.text = node.text.replace(k, v)
                        udfb_updated = True
    
    if prog_updated:
        try:
            prog_tree.write(PROGRAM_FILENAME)
        except PermissionError as e:
            print("Can't open " + str(PROGRAM_FILENAME) + " for editing")
            print(str(e))
        else:
            print("Write back to " + PROGRAM_FILENAME)        
    if udfb_updated:
        try:
            tree.write(BLOCK_FILENAME)
        except PermissionError as e:
            print("Can't open " + str(BLOCK_FILENAME) + " for editing")
            print(str(e))
        else:
            print("Write back to " + BLOCK_FILENAME)

if __name__ == "__main__":
    main()
