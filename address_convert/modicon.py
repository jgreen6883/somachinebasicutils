#! /usr/bin/env python
# coding: utf-8

"""
modicon.py

    This holds some helpful functions used by convert.py to do its job.
    
"""

import enum

class NoMemoryPrefixException(Exception):
    pass
    
class MemoryType(enum.Enum):
    MEMORY_BIT = "M"
    MEMORY_WORD = "MW"
    MEMORY_DOUBLE = "MD"
    MEMORY_FLOAT = "MF"
    CONSTANT_WORD = "KW"
    CONSTANT_DOUBLE = "KD"
    CONSTANT_FLOAT = "KF"
    TIMER = "TM"
    COUNTER = "C"

MEMTYPE_PREFIXES = {
    MemoryType.MEMORY_BIT : "%M",
    MemoryType.MEMORY_WORD : "%MW",
    MemoryType.MEMORY_DOUBLE : "%MD",
    MemoryType.MEMORY_FLOAT : "%MF",
    MemoryType.CONSTANT_WORD : "%KW",
    MemoryType.CONSTANT_DOUBLE : "%KD",
    MemoryType.CONSTANT_FLOAT : "%KF",
    MemoryType.TIMER : "%TM",
    MemoryType.COUNTER : "%C",
}

VALID_MEMTYPES = [
    MemoryType.MEMORY_BIT, 
    MemoryType.MEMORY_WORD,
    MemoryType.MEMORY_DOUBLE,
    MemoryType.MEMORY_FLOAT,
    MemoryType.CONSTANT_WORD,
    MemoryType.CONSTANT_DOUBLE,
    MemoryType.CONSTANT_FLOAT,
    MemoryType.TIMER,
    MemoryType.COUNTER,
]

def get_memtype_for_user(mtype_str):
    try:
        user_memtype = MemoryType(mtype_str)
    except ValueError as e:
        return mtype_str
    else:
        return user_memtype

def get_memory_prefix(mtype):
    if mtype in MEMTYPE_PREFIXES:
        return MEMTYPE_PREFIXES[mtype]
    else:
        raise NoMemoryPrefixException("No prefix for " + str(mtype) + " object")

def is_memory_word(addr):
    return addr[:3] == MEMTYPE_PREFIXES[MemoryType.MEMORY_WORD]

def is_memory_double(addr):
    return addr[:3] == MEMTYPE_PREFIXES[MemoryType.MEMORY_DOUBLE]

def is_memory_float(addr):
    return addr[:3] == MEMTYPE_PREFIXES[MemoryType.MEMORY_FLOAT]

def is_memory_bit(addr):
    return (
        addr[:2] == MEMTYPE_PREFIXES[MemoryType.MEMORY_BIT] and
        not is_memory_word(addr) and
        not is_memory_double(addr) and
        not is_memory_float(addr)
    )

def is_timer(addr):
    return addr[:3] == MEMTYPE_PREFIXES[MemoryType.TIMER]

def is_constant_word(addr):
    return addr[:3] == MEMTYPE_PREFIXES[MemoryType.CONSTANT_WORD]

def is_constant_double(addr):
    return addr[:3] == MEMTYPE_PREFIXES[MemoryType.CONSTANT_DOUBLE]

def is_constant_float(addr):
    return addr[:3] == MEMTYPE_PREFIXES[MemoryType.CONSTANT_FLOAT]

def is_counter(addr):
    return addr[:2] == MEMTYPE_PREFIXES[MemoryType.COUNTER]
